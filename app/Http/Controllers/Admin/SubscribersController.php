<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Models\Components\Users;

class SubscribersController extends Controller
{
	public function index(){
	//view all users	
		$user_model = new Users();
		$users = $user_model->getAllSubscribers();
		$data = array(
				'users' => $users
		);
		return view('admin.users.subscribers.index')->with($data);
	}
	
	public function activate($id)
	{
		$deleteItem = Users::find($id);
		$deleteItem->status = 1;
		$deleteItem->save();
		 
		\Session::flash('flash_message', 'User was activated in your list!');
		\Session::flash('alert-class', 'alert-success');
		 
		return redirect()->back();
	}
	
	public function deactivate($id)
	{
		$deleteItem = Users::find($id);
		$deleteItem->status = 0;
		$deleteItem->save();
		 
		\Session::flash('flash_message', 'User was deactivated in your list!');
		\Session::flash('alert-class', 'alert-danger');
		 
		return redirect()->back();
	}
	
	
	
}