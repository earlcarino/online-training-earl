<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Models\Components\Games;

class GamesController extends Controller
{
	public function index(){
	//view all users	
		$games = Games::get();
		$data = array(
				'games' => $games
		);
		return view('admin.games.index')->with($data);
	}
	public function activate($id)
	{
		$deleteItem = Games::find($id);
		$deleteItem->status = 1;
		$deleteItem->save();
			
		\Session::flash('flash_message', 'User was activated in your list!');
		\Session::flash('alert-class', 'alert-success');
			
		return redirect()->back();
	}
	
	public function deactivate($id)
	{
		$deleteItem = Games::find($id);
		$deleteItem->status = 0;
		$deleteItem->save();
			
		\Session::flash('flash_message', 'User was deactivated in your list!');
		\Session::flash('alert-class', 'alert-danger');
			
		return redirect()->back();
	}
	
	private function validator(Request $request)
	{
		$validator = Validator::make($request->except('_token'), [
				'id' => 'required',
				'name' => 'required',
				'description' => 'required'
		]);
	
		return $validator;
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = $this->validator($request);
		if ($validator->fails()) {
			\Session::flash('flash_message', $validator->errors());
			\Session::flash('alert-class', 'alert-danger');
	
			return response()->json(['success'=>'false']);
		}
		 $game = new Games($request->except('_token'));
		//if product image is not empty
		if (Input::hasFile('image')) {
			$file = Input::file('image');
		
			$filename  = time() . '.' . $file->getClientOriginalExtension();
			//copy image and resize as thumbnail
			$path_small = public_path("images/small/" . $filename);
			Image::make($file->getRealPath())->resize(69, 75)->save($path_small);
			//copy image and resize as medium size image
			$path_medium = public_path('images/medium/' . $filename);
			Image::make($file->getRealPath())->resize(146, 160)->save($path_medium);
			//copy image and resize as large size image
			$path_large = public_path('images/large/' . $filename);
			Image::make($file->getRealPath())->resize(457, 500)->save($path_large);
		
			//$file->move('product_images', $file->getClientOriginalName());
			$game->img_large = 'images/large/'.$filename;
			$game->img_small = 'images/small/'.$filename;
			$game->img_medium = 'images/medium/'.$filename;
		}
		$game->status = 1;
	
		if ($game->save()) {
			\Session::flash('flash_message', 'Game was added to your list!');
			\Session::flash('alert-class', 'alert-success');
			 
			return response()->json(['success'=>'true']);
		}
		\Session::flash('flash_message', 'There is an error while saving the data!');
		\Session::flash('alert-class', 'alert-danger');
		 
		return response()->json(['success'=>'false']);
	}
	
	public function edit($id){
		$user = Games::find($id);
		return response()->json(['success'=>'true','data' => $user]);
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$id = Input::get('id');
			$validator = Validator::make($request->except('_token'), [
				'id' => 'required',
				'name' => 'required',
				'description' => 'required',
		]);
		if ($validator->fails()) {
			\Session::flash('flash_message', $validator->errors());
			\Session::flash('alert-class', 'alert-danger');
	
			return response()->json(['success'=>'false']);
		}

		$game = Games::find($id);
		$game->name = Input::get('name');
		$game->description = Input::get('description');
		
		if ($game->save()) {
			\Session::flash('flash_message', 'Game was updated successfully!');
			\Session::flash('alert-class', 'alert-success');
			 
			return response()->json(['success'=>'true']);
		}
		\Session::flash('flash_message', 'There is an error while saving the data!');
		\Session::flash('alert-class', 'alert-danger');
		 
		return response()->json(['success'=>'false']);
	}

}