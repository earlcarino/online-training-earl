<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Models\Components\Users;
use App\Models\Components\UserRoles;

class UsersController extends Controller
{
	public function index(){
	//view all users	
		$user_model = new Users();
		$users = $user_model->getAllUsers();
		$usertype = UserRoles::where('active','=',1)->get();
		$data = array(
				'users' => $users,
				'usertype' => $usertype
		);
		return view('admin.users.index')->with($data);
	}
	
	public function activate($id)
	{
		$deleteItem = Users::find($id);
		$deleteItem->status = 1;
		$deleteItem->save();
		 
		\Session::flash('flash_message', 'User was activated in your list!');
		\Session::flash('alert-class', 'alert-success');
		 
		return redirect()->back();
	}
	
	public function deactivate($id)
	{
		$deleteItem = Users::find($id);
		$deleteItem->status = 0;
		$deleteItem->save();
		 
		\Session::flash('flash_message', 'User was deactivated in your list!');
		\Session::flash('alert-class', 'alert-danger');
		 
		return redirect()->back();
	}
	
	private function validator(Request $request)
	{
		$validator = Validator::make($request->except('_token'), [
				'id' => 'required',
				'lastname' => 'required',
				'firstname' => 'required',
				'password' => 'required',
				'email_address' => 'required|unique:users',
				'username' => 'required|unique:users'
		]);
	
		return $validator;
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = $this->validator($request);
		if ($validator->fails()) {
			\Session::flash('flash_message', $validator->errors());
			\Session::flash('alert-class', 'alert-danger');
	
			return response()->json(['success'=>'false']);
		}
		$token_code = str_random(50);
		$user = new Users;
		$user->token_code = $token_code;
		$user->firstname = Input::get('firstname');
		$user->middle_initial = Input::get('middlename');
		$user->lastname = Input::get('lastname');
		$user->username = Input::get('username');
		$user->password = bcrypt(Input::get('password'));
		$user->user_role = Input::get('user_role');
		$user->email_address = Input::get('email_address');
		$user->status = 0;

		if ($user->save()) {
			\Session::flash('flash_message', 'User was added to your list!');
			\Session::flash('alert-class', 'alert-success');
			 
			return response()->json(['success'=>'true']);
		}
		\Session::flash('flash_message', 'There is an error while saving the data!');
		\Session::flash('alert-class', 'alert-danger');
		 
		return response()->json(['success'=>'false']);
	}
	
	public function edit($id){
		$user = Users::find($id);
		return response()->json(['success'=>'true','data' => $user]);
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$id = Input::get('id');
			$validator = Validator::make($request->except('_token'), [
				'id' => 'required',
				'lastname' => 'required',
				'firstname' => 'required',
				'username' => 'required|unique:users,username,'.$id,
	            'email_address' => 'required|email|unique:users,email_address,'.$id,
		]);
		if ($validator->fails()) {
			\Session::flash('flash_message', $validator->errors());
			\Session::flash('alert-class', 'alert-danger');
	
			return response()->json(['success'=>'false']);
		}

		$user = Users::find($id);
		$user->firstname = Input::get('firstname');
		$user->middle_initial = Input::get('middlename');
		$user->lastname = Input::get('lastname');
		$user->username = Input::get('username');
		$user->user_role = Input::get('user_role');
		$user->email_address = Input::get('email_address');
		
		if ($request->password != '') {
			$user->password = bcrypt(Input::get('password'));
		}

		if ($user->save()) {
			\Session::flash('flash_message', 'User was updated successfully!');
			\Session::flash('alert-class', 'alert-success');
			 
			return response()->json(['success'=>'true']);
		}
		\Session::flash('flash_message', 'There is an error while saving the data!');
		\Session::flash('alert-class', 'alert-danger');
		 
		return response()->json(['success'=>'false']);
	}
	
 /**
     * Loads Edit Profile page
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editProfile(Request $request)
    {
        $userId = Auth::id();
        $firstname = Auth::user()->firstname;
        $lastname = Auth::user()->lastname;
        $username = Auth::user()->username;
        $email_address = Auth::user()->email_address;
        $middle_initial = Auth::user()->middle_initial;

        //echo $userId."</br>";

        //exit;
        $data = array(
            //'settings' => 1,
            'url' => 'update-profile/',
            'user_id' => $userId,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'username' => $username,
            'email_address' => $email_address,
            'middle_initial' => $middle_initial,
            'method' => 'POST',
            'title' => 'Edit Profile'
        );

        //return view('auth.editprofile');
        return view('auth.editprofile')->with($data);
    }

    /**
     * Save profile changes in the database
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $user_id = $request->user_id;
            $validator = Validator::make($request->except('_token'), [
                'username' => 'required|unique:users,username,'.$user_id,
                'firstname' => 'required|regex:/^[A-Za-z\s-_]+$/',
                'lastname' => 'required|regex:/^[A-Za-z\s-_]+$/',
                'email_address' => 'required|email|unique:users,email_address,'.$user_id
            ]);
       
        if ($validator->fails()) {
            //get all the errors;
            $error = $validator->errors()->all();
            $msg = '';
            $counter = 0;
            foreach ($error as $i => $v) {
                $msg .= $v . ' ';

                $counter++;
            }
            //return redirect()->back()->withErrors($validator); //original

            return redirect()->back()->withErrors($validator)->with('error', 'Error')->withInput();
        } else {
            $user = Users::where('id', $request->user_id)->first();
            $user->username= isset($request->username) ? $request->username : '';
            $user->firstname= isset($request->firstname) ? $request->firstname : '';
            $user->lastname= isset($request->lastname) ? $request->lastname : '';
            $user->email_address= isset($request->email_address) ? $request->email_address : '';
            $user->middle_initial= isset($request->middle_initial) ? $request->middle_initial : '';
            

            if ($request->password != '') {
                $user->password = bcrypt($request->password);
            }
            $user->save();
           
            
            \Session::flash('flash_message', 'Profile was successfully updated!');
            \Session::flash('alert-class', 'alert-success');
            return redirect('/edit-profile');
        }
    }
	
}