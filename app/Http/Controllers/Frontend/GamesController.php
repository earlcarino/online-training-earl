<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Models\Components\GameResults;
use App\Models\Components\Users;
use App\Models\Components\Games;

class GamesController extends Controller
{
	public function index(){
	}
    
    public function gameOne(){
	   //9x2 game
		$data = array(
            'gameSettings' => Array("inProgress" => true, "currentLevel" => 0)
		);
		return view('frontend.games.GameNineTwo')->with($data);
	}
    
    public function store() {
        $user = auth()->user();
        $gameResultId = GameResults::where('userId',"=",$user->id)
                        ->first(['id']);
        $gameResultCount = GameResults::where('userId',"=",$user->id)
                        ->count();
        
        if($gameResultCount == 0) {
            $game_results           = new GameResults;
            $game_results->userId   = 2;
            $game_results->level    = $request->data['level'];
            $game_results->step     = $request->data['step'];
            $game_results->process  = $request->data['process'];
            $game_results->status   = $request->data['status'];
            $game_results->life     = count(array_filter(($request->data['life'])));
            $game_results->gameTime = $request->data['gameTime'];
            $game_results->status   = $request->data['status'];
            $message                = "add";
        } else {
            $game_results           = GameResults::find($gameResultId->id);
            $game_results->userId   = $user->id;
            $game_results->level    = $request->data['level'];
            $game_results->step     = $request->data['step'];
            $game_results->process  = $request->data['process'];
            $game_results->status   = $request->data['status'];
            $game_results->life     = count(array_filter(($request->data['life'])));
            $game_results->gameTime = $request->data['gameTime'];
            $game_results->status   = $request->data['status'];
            $message                = "update";
        }
        
        if($game_results->save()){
            return response()->json(['success'=>'true']);
        } else {
            return response()->json(['success'=>'false']);
        }
    }

}