<?php

namespace App\Models\Components;

// use Illuminate\Database\Eloquent\Model;
use DB;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

class Users extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
		'firstname',
    	'lastname',
        'username',
        'email_address',
    	'user_role',
        'password',
        'token_code',
        'last_login_at',
    	'token_code'
    ];

    protected $hidden = ['password'];
    protected $guarded = ['id'];
    
    public function getAllUsers(){
    	$data = DB::table('users')
    	->select(DB::raw("users.*,
   				(SELECT name FROM roles WHERE id = users.user_role) AS user_role
            "))
            	->where('users.user_role',1)
                ->get();
                return $data;
    }
    
    public function getAllPartners(){
    	$data = DB::table('users')
    			->select(DB::raw("users.*"))
                ->where('users.user_role',2)
                ->get();
                return $data;
    }
    
    public function getAllSubscribers(){
    	$data = DB::table('users')
    			->select(DB::raw("users.*"))
                ->where('users.user_role',3)
                ->get();
                return $data;
    }

}
