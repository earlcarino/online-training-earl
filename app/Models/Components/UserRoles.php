<?php

namespace App\Models\Components;

// use Illuminate\Database\Eloquent\Model;
use DB;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

class UserRoles extends Authenticatable
{

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = "roles";
    protected $guarded = ['id'];
    
    public function users()
    {
    	return $this->hasMany(User::class, 'user_role');
    }


   
}
