<?php

namespace App\Models\Components;

// use Illuminate\Database\Eloquent\Model;
use DB;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

class GameResults extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "game_results";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
		"userId",
        "level",
        "step",
        "process",
        "life",
        "gameTime",
        "status"
    ];

    protected $guarded = ['id'];
    

}
