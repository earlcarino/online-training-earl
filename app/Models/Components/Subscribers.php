<?php

namespace App\Models\Components;

// use Illuminate\Database\Eloquent\Model;
use DB;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

class Subscribers extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "subscribers";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
		'user_id',
    	'reffered_by',
    	'package_id',
        'package_startdate',
    	'package_startend'
    ];

    protected $guarded = ['id'];
    

}
