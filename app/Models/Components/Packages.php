<?php

namespace App\Models\Components;

// use Illuminate\Database\Eloquent\Model;
use DB;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

class Packages extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "subscription_package";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
		'package_name',
    	'description',
    	'monthly_fee',
        'annual_fee'
    ];

    protected $guarded = ['id'];
    
     public function displayPackage(){
        $data = DB::table('subscription_package')
       // ->join('package_games','package_games.package_id','subscription_package.id')
        ->select(DB::raw("*"))
        ->where('status','=',1)
        ->get();
        return $data;
    }

}
