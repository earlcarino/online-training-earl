<?php

namespace App\Models\Components;

// use Illuminate\Database\Eloquent\Model;
use DB;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

class Partners extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "partners";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
		'user_id',
    	'company_name',
    	'referral_id',
        'employer_id_number'
    ];

    protected $guarded = ['id'];
    

}
