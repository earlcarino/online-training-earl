<?php

use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'email_address' => str_random(10).'@gmail.com',
            'token_code' => str_random(10),
            'password' => bcrypt('secret'),
            'firstname' => 'admin',
            'middle_initial' => 'admin',
            'lastname' => 'admin',
            'user_role' => 1,
            'username' => 'admin',
            'status' => 1,
        ]);
        
        DB::table('roles')->insert([
            'name' => 'administrator',
            'active' => 1
        ]);
    }
}
