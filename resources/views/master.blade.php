<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ config('app.name') }} | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        @include('_partials.styles')
        @yield('styles')
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            window.Laravel = { "csrfToken": "{{csrf_token()}}" };
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>{{ config('app.name') }}</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>{{ config('app.name') }}</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                           
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('dist/img/avatar5.png') }}" class="user-image" alt="User Image">
                                <span class="hidden-xs">Reshiel Ann Ricalde</span>
                                {{--<span class="hidden-xs">{{ Auth::user()->firstname." ".Auth::user()->lastname }}</span>--}}
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="{{ asset('dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
                                        <p>
                                            {{-- Auth::user()->firstname." ".Auth::user()->lastname --}}
                                            <small>Member since {{--  date('M Y', strtotime(Auth::user()->created_at)) --}}</small>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="{{ url('/edit-profile') }}" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            @include('sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @if(!empty(Session::get('message')))
                <div class="well">
                    <p class="alert {{ Session::pull('alert-class', 'alert-info') }}">{!! Session::pull('message') !!}</p>
                </div>
                @endif
                
                @yield('container')
            </div>
            @include('footer')
            <!-- Add the sidebar's background. This div must be placed
                immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        @include('_partials.scripts')
        @yield('scripts')
    </body>
</html>