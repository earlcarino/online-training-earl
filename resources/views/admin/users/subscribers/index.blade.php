   @extends('master')
  @section('container')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Subscribers List
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#"><i class="fa fa-users"></i> Users Management</a></li>
        <li class="active">Subscribers List</li>
      </ol>
	    <!-- Main content -->
    </section>

    <!-- Main content -->
    <section class="content">
     @if(Session::has('flash_message'))
    <div class="add_offer_holder alert {{ Session::get('alert-class') }}">
        <span>{{ Session::get('flash_message') }}</span> <br/>
    </div>
    @endif
    @if(count($errors->all())>0)
    <div class="alert alert-danger">
        @foreach($errors->all() as $err)
        {{$err}}<br/>
        @endforeach
    </div>
    @endif
    
      <div class="row">
        <div class="col-xs-19">
          <div class="box">
            <div class="box-header">
             
            
            <!-- /.box-header -->
            <div class="box-body">
				 <!--<a class="btn btn-info" id="btn_add_new"><i class="fa fa-user-plus"></i> Add new</a><br/>-->
			<table>
			 <div class="box-body">
              <table id="example1" class="table table-bordered table-striped" id="tbl_roles">
    <thead>
        <tr>
			<th>#</th>
            <th>Fullname</th>
            <th>Username</th>
            <th>Email Address</th>
            <th>Status</th>
            <th colspan='4'>Action</th>
        </tr>
    </thead>
    <tbody>
        <!--Use a while loop to make a table row for every DB row-->
        <?php $x = 1;?>
		  @foreach( $users as $user )
                        <tr>
                             <td > {{ $x }}</td>
                            <td > {{ $user->firstname }} {{ $user->middle_initial }} {{ $user->lastname }}</td>
                            <td > {{ $user->username }}</td>
                            <td > {{ $user->email_address }}</td>
                            <td > 
                            @if($user->status == 1)
                            	<label class="label label-success">Active</label>
                            @else 
                            	<label class="label label-warning">Inactive</label>
                            @endif </td>
                           
                            <td>
                              @if($user->status == 1)
                                <form  action="{{ url('users/deactivate/'.$user->id) }}" method="post"> 
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      
                                    <button type="submit" class='btn btn-danger'  name="btnDelete" onclick="return confirm('Are you sure you want to deactivate this user?');">
                                         Deactivate
                                    </button> 
                                    
                                </form>
   								  @else 
                                <form  action="{{ url('users/activate/'.$user->id) }}" method="post"> 
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      
                                    <button type="submit" class='btn btn-success'  name="btnDelete" onclick="return confirm('Are you sure you want to activate this user?');">
                                        Activate
                                    </button> 
                                    
                                </form>
                                @endif 
                            </td>
                             <td> <a href="javascript:void(0)" id="btn-edit-role" data-id="{{ $user->id }}" class='btn btn-primary btn-edit-role'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
							 </td>
           <?php $x++; ?>
         @endforeach
       
    </tbody>
</table>

                </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      </div>
      <!-- /.row -->
    </section>
  
@stop
@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
@stop
@section('scripts')
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
@stop