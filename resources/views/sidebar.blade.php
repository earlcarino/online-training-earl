<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->firstname." ".Auth::user()->lastname }}<br>
                 <i>ABC Company</i></p>
                
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
               
            </div>
        </div>
        <!-- search form -->
        <form action="{{-- route('search') --}}" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <!-- Admin menus -->
              <li class="treeview {{-- Request::is('dashboard') ? 'active' : '' --}}">
                <a href="{{-- url('/dashboard') --}}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                <span class="pull-right-container">
                </span>
                </a>
            </li>
            <li class="treeview {{ Request::is(['users', 'roles','partners','subscribers']) ? 'active' : '' }}">
                <a href="#">
                <i class="fa fa-users"></i> <span>Users Management</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('users') ? 'active' : '' }}">
                        <a href="{{ route('users') }}"><i class="glyphicon glyphicon-user"></i> Users</a>
                    </li>
                    <li class="{{ Request::is('roles') ? 'active' : '' }}">
                        <a href="{{ route('roles') }}"><i class="glyphicon glyphicon-knight"></i> Roles</a>
                    </li>
                     <li class="{{ Request::is('subscribers') ? 'active' : '' }}">
                        <a href="{{ route('subscribers') }}"><i class="fa fa-user"></i> Subscribers</a>
                    </li>
                     <li class="{{ Request::is('partners') ? 'active' : '' }}">
                        <a href="{{ route('partners') }}"><i class="fa fa-user"></i> Partners</a>
                    </li>
                </ul>
            </li>
			<li class="{{ Request::is(['game']) ? 'active' : '' }}">
                <a href="#">
                <i class="fa fa-gears"></i> <span>Game Management</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu"> 
                    <li class="{{ Request::is('games') ? 'active' : '' }}">
                        <a href="{{ route('games') }}"><i class="fa fa-gamepad"></i> Games</a>
                    </li>
                    <li class="{{ Request::is('gameResults') ? 'active' : '' }}">
                        <a href="{{ route('gameResults') }}"><i class="fa fa-gamepad"></i> Game Results</a>
                    </li>
                    
                </ul>
            </li>
          <li class="{{ Request::is('packages') ? 'active' : '' }}">
                        <a href="{{ route('packages') }}"><i class="fa fa-cubes"></i> Subscription Package</a>
                    </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>