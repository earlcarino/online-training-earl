@extends('master')

@section('container')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        My Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">My Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    @if(Session::has('flash_message'))
        <div class="edit_profile_holder alert {{ Session::get('alert-class') }}">
            <span>{{ Session::get('flash_message') }}</span> <br/>
        </div>
    @endif

    @if(count($errors->all())>0)
        <div class="alert alert-danger">
            @foreach($errors->all() as $err)
                {{$err}}<br/>
            @endforeach
        </div>
    @endif

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
    <div class="box">
            <div class="box-header with-border">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <form action="{{url($url)}}" method="post" class="form-horizontal">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input name="_method" type="hidden" value="{{$method}}">
                {{--<input type="hidden" name="status" value="1">--}}
                <input type="hidden" id="user_id" name="user_id" value="{{$user_id}}">
              <div class="box-body">
                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">

                              <label for="username" class="col-sm-3 control-label">Username</label>

                              <div class="col-sm-9">
                                  {{--<input type="text" class="form-control" id="firstname" maxlength="128" name="firstname" placeholder="First Name" value="{{isset($products) ? $products->product_name : ''}}">--}}
                                  <input type="text" class="form-control" id="username" name="username" maxlength="128" placeholder="Username" value="{{ old('username', $username!='' ? $username : '') }}" >
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">

                              <label for="firstname" class="col-sm-3 control-label">First Name</label>

                              <div class="col-sm-9">
                                  {{--<input type="text" class="form-control" id="firstname" maxlength="128" name="firstname" placeholder="First Name" value="{{isset($products) ? $products->product_name : ''}}">--}}
                                  <input type="text" class="form-control" id="firstname" maxlength="128" name="firstname" placeholder="First Name" value="{{ old('firstname', $firstname!='' ? $firstname : '') }}">
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                       <div class="col-sm-6">
                          <div class="form-group">

                              <label for="middle_initial" class="col-sm-3 control-label">Middle Initial</label>

                              <div class="col-sm-9">
                                  <input type="text" class="form-control" id="middle_initial" name="middle_initial" maxlength="1" placeholder="Middle Initial" value="{{ old('middle_initial',isset($middle_initial) ? $middle_initial : '') }}">
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">

                              <label for="lastname" class="col-sm-3 control-label">Last Name</label>

                              <div class="col-sm-9">
                                  {{--<input type="text" class="form-control" id="firstname" maxlength="128" name="firstname" placeholder="First Name" value="{{isset($products) ? $products->product_name : ''}}">--}}
                                  <input type="text" class="form-control" id="lastname" maxlength="128" name="lastname" placeholder="Last Name" value="{{ old('lastname', isset($lastname) ? $lastname : '') }}">
                              </div>
                          </div>
                      </div>
                  </div>
                 
                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">

                              <label for="email_address" class="col-sm-3 control-label">Email Address</label>

                              <div class="col-sm-9">
                                  <input type="email" class="form-control" id="email_address" name="email_address" maxlength="128" placeholder="Email Address" value="{{ old('email_address',isset($email_address) ? $email_address : '') }}">
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">

                              <label for="password" class="col-sm-3 control-label">Password</label>

                              <div class="col-sm-9">
                                  <input type="password" class="form-control" id="password" maxlength="128" name="password" placeholder="Enter New Password Here" title="Leave it blank if you do not want to change your password" value="">
                              </div>
                          </div>
                      </div>
                  </div>
  		
              </div>
             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <div class="pull-right btn-toolbar">
                      <button type="submit" name="btnSubmit" class="btn btn-info">Save Changes</button>
                      <button id="btnCancel" name="btnCancel" class="btn btn-default">Cancel</button>
                  </div>
                {{--<a type="submit" class="btn btn-default" href="{{ url('product') }}">Cancel</a>--}}
                {{--<button type="submit" name="btnSubmit" class="btn btn-info pull-right">Save Changes</button>--}}
              </div>
              <!-- /.box-footer -->
            </form>
            </div>

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
@stop
 