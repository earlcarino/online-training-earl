<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Lift Brain Training | Subscribe Now!</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset("bootstrap/css/bootstrap.min.css") }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset("dist/css/AdminLTE.min.css") }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset("plugins/iCheck/square/blue.css") }}">
  
    <link rel="stylesheet" href="{{ asset("css/package.css") }}">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box" style="width:100% !important;margin-left: 15%;">
  <div class="login-logo" style="text-align: left;margin-left: 30%;">
  <b>Select Subscription Package</b>
  </div>
  
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
       @foreach($packages as $keys => $package)
        <div class="col-xs-12 col-md-3">
          <div class="panel panel-success">
               @if($keys == 1) 
                <div class="cnrflash">
                    <div class="cnrflash-inner">
                        <span class="cnrflash-label">MOST
                            <br>
                            POPULR</span>
                    </div>
                </div>
                @endif 
                <div class="panel-heading">
                    <h3 class="panel-title">
                        {{ $package->package_name }}</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            ${{ $package->monthly_fee }}<span class="subscript">/mo</span></h1>
                       <!--  <small>1 month FREE trial</small> -->
                    </div>
                    <table class="table">
                     <tr class="active">
                            <td>
                               Games Included are:
                            </td>
                        </tr>
                    @foreach($package->games as $key => $game)
                    
                    <tr>
                            <td>
                              {{ $game->name }}
                            </td>
                        </tr>
                    @endforeach

                    </table>
                </div>
                <div class="panel-footer">
                    <a href="{{ url('/registration/'.$package->id) }}" class="btn btn-success" role="button">Sign Up</a>
                   </div>
            </div>
        </div>
       @endforeach
    </div>
</div>


<!-- jQuery 2.2.3 -->
<script src="{{ asset("plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset("bootstrap/js/bootstrap.min.js") }}"></script>
<!-- iCheck -->
<script src="{{ asset("plugins/iCheck/icheck.min.js") }}"></script>
</body>
</html>