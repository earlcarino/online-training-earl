	$(function (){
		var myModal = $("#myModal"),			
		btn_add_new = $("#btn_add_new"),
		btn_save_changes = $("#btn_save_changes"),
		btn_close_modal = $("#btn_close_modal"),
		myModalLabel = $("#myModalLabel"),
		form_roles = $("#form_roles"),
		package_name = $("#package_name"),
		description = $("#description"),
		monthly_fee = $("#monthly_fee"),
		annual_fee = $("#annual_fee"),
		role_id = $("#role_id");
		
		
		btn_add_new.click(function (){
			myModalLabel.html("Add New Subscription Package");
			myModal.modal('show'); 
			role_id.val("new");
		});
		
		$(document).on("click", ".btn-edit-role", function (){
			var $this = $(this), 
				id = $this.attr("data-id");
				
			myModalLabel.html("View/Edit Subscription Package");	
			role_id.val(id);
			
			$.ajax({
				url: '/packages/'+id,
				type: 'get',
				dataType: 'json',
				success: function (result){
					var res = result.data;
					package_name.val( res.package_name );
					description.val( res.description );
					monthly_fee.val( res.monthly_fee );
					annual_fee.val( res.annual_fee );
						role_id.val( res.id );
						myModal.modal('show');
				
				}
			});
			myModal.modal('show');
		});
		
		form_roles.on('submit', function (e){
			var $this = $(this),
				requestType = 'patch',
				action = 'update';

			e.preventDefault();

			if( role_id.val() == 'new' ){
				requestType = 'post';
				action = 'store';
			}

			$.ajax({
				url: '/packages/'+action,
				type: requestType,
				dataType: 'json',
				data: $this.serialize(),
				beforeSend: function (){
					btn_save_changes.addClass("disabled").attr("disabled", "disabled");
				},
				success: function (data){
					myModal.modal('show'); 
					location.reload(); // then reload the page.(3)
				},
				complete: function (){
					btn_save_changes.removeClass("disabled").removeAttr("disabled");
				}
			});
		});
		
	

});